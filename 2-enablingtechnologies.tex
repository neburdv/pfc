
\chapter{Enabling Technologies}
\label{chap:enabling_technologies}
\begin{chapterintro}
This chapter introduces which technologies have made possible this project. First of all we must introduce Linked Data and, specifically, GeoLinked Data~\cite{geolinkeddata1, geolinkeddata2} and all its possibilities.Then we present Sefarad\footnote{https://github.com/gsi-upm/Sefarad}, an HTML5 Framework developed by \textit{Grupo de Sistemas Inteligentes} (GSI\footnote{http://www.gsi.dit.upm.es/}) which provides us a semantic front end to Linked Data (LOD)~\cite{robertobermejo}. Finally, we introduce the other technologies that have helped us to develop this project. 
\end{chapterintro}

\cleardoublepage

\section{Overview}
Linked Data is a technological innovation that transforms the way we think about information and its role in society, in our case geographic information. Linked Data has been recently suggested as one of the best alternatives for creating these shared information spaces \cite{geolinkeddata3}. Linked Data describes a method of publishing structured and related data so that it can be interlinked and become more useful, which results in the Semantic Web\footnote{http://www.w3.org/standards/semanticweb/} (or Web of Data). It builds upon standard Web technologies such as HTTP, RDF and URIs, but rather than using them to serve web pages for human readers, it extends them to share information in a way that can be read automatically by computers. This enables data from different sources to be connected and queried using SPARQL standard. This is specially important for sophisticated types of information, in particular information with spatial and temporal components.

\begin{figure}[ht!]
\centering
\includegraphics[width=400pt]{img/lodcloud.png}
\caption[Linked Open Data cloud ]{Linked Open Data cloud diagram example\footnotemark}
\label{fig:lodcloud}
\end{figure}

\footnotetext{http://lod-cloud.net/}
With the adoption of Linked Data, the traditional complexities of conceptual database schemata for spatial data can safely remain internal to organizations. Their externally relevant contents get streamlined into the open and more manageable form of vocabulary definitions. Users of Linked Data do not need to be aware of complex schema information to use data adequately, but ”only” of the semantics of types and predicates (such as isLocatedIn) occurring in the data. While many questions remain to be answered about how to produce and maintain vocabulary specifications, the elaborate layering of syntactic, schematic, and semantic interoperability issues has simplified to a single common syntax (RDF\footnote{http://www.w3.org/RDF/}), the irrelevance of traditional schema information outside a database, and a focus on specifying and sharing vocabularies.

This simplification is more dramatic for spatially and temporally referenced data (with their complexities in the form of geometries and scale hierarchies). The resulting paradigm shift, from distributed complex databases accessed through web services that expose schemata to knowledge represented as graphs, whose links can be given well-defined meaning, radically changes some of the long-standing problems of GIScience and GIS practice. Everything said is a way to facilitate analysis and integration of all geographic information available worldwide. 

This master thesis describes the creation of a web application for GeoLinked Data management and visualization. The main goal of this project is to develop a web application that facilitates the handling and visualization of GeoLinked Data available worldwide. Users could manage geographical information from any SPARQL endpoint just running their own queries or graph their own datasets storing them in a semantic web server such as Linked Media Framework (LMF\footnote{https://code.google.com/p/lmf/}) or in a NoSQL database such as MongoDB\footnote{http://www.mongodb.org/}.


\section{Linked Data in a Nutshell}
\label{sec:linkeddata}

The rise of the Open Data Movement has led to the Web of Data grow significantly over the last years. This Web of Data has started to span data sources form a wide range of domains such as people, companies, music, scientific publications, etc. The principles of Linked Data were first outlined by Berners-Lee in 2006 \cite{linkeddataprinciples}:

\begin{enumerate}
\item Use URIs as names for things.
\item Use HTTP URIs so that people can look up those names.
\item When someone looks up a URI, provide useful information, using the standards (RDF, SPARQL).
\item Include links to other URIs. so that they can discover more things.
\end{enumerate}

Linked Data is the name for a collection of design principles, practices and technologies centered around a novel paradigm to expose, publish, retrieve, reuse, and integrate data on the Web. In summary, that is simply about using the Web to create typed links between data from different sources. In contrast to the Document Web, the Semantic Web aims at establishing named and directed links between typed data. For example, a normal Web page about Portsmouth (such as http://en.wikipedia.org/wiki/Portsmouth) may link to another page about Hampshire (such as http://en.wikipedia.org/wiki/Hampshire). For a machine, the intended meaning of such links is difficult to interpret and the Web pages can only be consumed as integral units of text or other media. On the Linked Data Web, by contrast, the link between Portsmouth and Hampshire would be directed and labelled, for example, forming the statement that Portsmouth is located in Hampshire. Additionally, the two places would be typed, e.g., as city and county, jointly leading to the statement that the city of Portsmouth is located in the county of Hampshire. Finally, the predicate isLocatedIn could be defined as a transitive relation in an ontology. Thus, in conjunction with a statement that Hampshire county is located in the UK, one could automatically derive the new statement that Portsmouth is located in the UK.
\\
\begin{lstlisting}[caption=RDF/XML document example, captionpos=b,language=rdf,basicstyle=\tiny ,frame=single]
<?xml version="1.0" encoding="utf-8"?>
<rdf:RDF xmlns:contact="http://www.w3.org/2000/10/swap/pim/contact#" xmlns:eric="http://www.w3.org/People/EM/contact#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="http://www.w3.org/People/EM/contact#me">
    <contact:fullName>Eric Miller</contact:fullName>
  </rdf:Description>
  <rdf:Description rdf:about="http://www.w3.org/People/EM/contact#me">
    <contact:mailbox rdf:resource="mailto:e.miller123(at)example"/>
  </rdf:Description>
  <rdf:Description rdf:about="http://www.w3.org/People/EM/contact#me">
    <contact:personalTitle>Dr.</contact:personalTitle>
  </rdf:Description>
</rdf:RDF>
\end{lstlisting}

Technically, Linked Data refers to data published on the Web in such a way that it is machine-readable, its meaning is explicitly defined, it is linked to other external data sets, and can in turn be linked to from external data sets. That three given elements constitute each piece of information in Linked Data, one refers to such statements as triples, consisting of a subject (Portsmouth), a predicate (isLocatedIn), and an object (Hampshire). This syntax, which happens to be the simplest form in which statements can be made in natural language, has thus been carried over to the world of data. The data model for triples is the so-called Resource Description Framework. Every entity in the physical world (even a subject, a predicate or an object) should be identified by a global unique URI, and all the information should be provided by using W3C standards such as mentioned RDF or OWL. 

Linked Data can be queried using SPARQL\footnote{http://www.w3.org/TR/rdf-sparql-query} (an acronym for SPARQL Protocol and RDF Query Language), a query language for RDF which became an official W3C Recommendation\footnote{http://www.w3.org/blog/SW/2008/01/15/}. The SPARQL query language consists of the syntax and semantics for asking and answering queries against RDF graphs and contains capabilities for querying by triple patterns, conjunctions, disjunctions, and optional patterns. It also supports constraining queries by source RDF graph and extensible value testing. Results of SPARQL queries can be ordered, limited and offset in number, and presented in several different forms, such as JSON, RDF/XML, etc.
\\
\begin{lstlisting}[caption=SPARQL query example, captionpos=b, label=lst:sparql,
   basicstyle=\ttfamily,frame=single]
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
SELECT ?name ?email
WHERE {
  ?person a foaf:Person.
  ?person foaf:name ?name.
  ?person foaf:mbox ?email.
}
\end{lstlisting}

In our particular case of dealing with geographic information, GeoSPARQL\footnote{http://www.opengeospatial.org/standards/geosparql} enriches SPARQL by quantitative reasoning. Linked Data is usually stored and accessed via SPARQL endpoints (e.g., DBpedia\footnote{http://dbpedia.org/} or GeoNames\footnote{http://www.geonames.org/}). The ontologies that allow human users and machines to understand which concepts and predicates can be queried, and how they are formally defined, are described using languages such as the Web Ontology Language (OWL\footnote{http://www.w3.org/2001/sw/wiki/OWL}).

In the geospatial context, GeoLinked Data\footnote{http://linkedgeodata.org/} is an open initiative whose aim is to enrich the Semantic Web with geospatial data into the context of INSPIRE\footnote{http://inspire.ec.europa.eu/} \textit{(INfrastructure for SPatial InfoRmation in Europe)} Directive. This initiative focuses its efforts to collect, process and publish geographic information from different organizations around the world and providing the suitable tools for handing all the data. 

\section{Sefarad}
\label{sec:sefarad}
Sefarad is a web application developed to explore linked data by making SPARQL queries to the chosen endpoint without writing more code, so it provides a semantic front-end to Linked Open Data ~\cite{robertobermejo}. It allows the user to configure his own dashboard with many different widgets to visualize, explore and analyse graphically the different characteristics, attributes and relationships of the queried data. Sefarad is developed in HTML5\footnote{http://www.w3.org/TR/html5/} and follows a Model View-View Model (MVVM) pattern performed with the Knockout\footnote{http://knockoutjs.com/} framework. This JavaScript library allows us to create responsive and dynamic interfaces which automatically is updated when the data changes. The different parts of the UI are connected to the data model by declarative bindings. 

Sefarad consists of two different tabs: dashboard and control panel. The first tab allows the user to perform faceted search on the data accessed, so the users can explore a collection of information by applying multiple filters. In the control panel tab statistics about the dataset are visualized.

\hspace{1cm}

\begin{figure}[ht]
\centering
\begin{minipage}[b]{0.45\linewidth}
\includegraphics[scale=0.15]{img/mainlayout.png}
\caption{Main Layout}
\label{fig:minipage1}
\end{minipage}
\quad
\begin{minipage}[b]{0.45\linewidth}
\includegraphics[scale=0.15]{img/dashboard.png}
\caption{Dashboard}
\label{fig:minipage2}
\end{minipage}
\end{figure}

The great potential of Sefarad for our project lies in the capability to create our own widgets really easily. We should not worry about obtaining the filtered data and updating the widget when a new facet is selected thanks to Knockout framework. For this purpose, the application specifies how to create a new Javascript file in which it should be placed a Javascript object using D3.js\footnote{http://d3js.org/} framework. We will take advantage of this feature to develop geographic widgets. The widget template is as follows:
\pagebreak
\begin{lstlisting}[language=JavaScript, captionpos=b, caption="Sefarad widget template"]
// New widget
var newWidget = {
    // Widget name.
    name: "Name",
    // Widget description.
    description: "description",
    // Path to the image of the widget.
    img: "path/to/image",
    // Type of the widget.
    type: "type",
    // Help display on the widget
    help: "help",
    // Category of the widget (1: textFilter, 2: numericFilter, 3: graph, 5:results, 4: other, 6:map)
    cat: X,

    render: function() {
        var id = 'A' + Math.floor(Math.random() * 10001);
        var field = newWidget.field || "";
        vm.activeWidgetsRight.push({
            "id": ko.observable(id),
            "title": ko.observable(newWidget.name),
            "type": ko.observable(newWidget.type),
            "field": ko.observable(field),
            "collapsed": ko.observable(false),
            "showWidgetHelp": ko.observable(false),
            "help": ko.observable(newWidget.help)
        });
        newWidget.paint(id);
    },

    paint: function(id) {
        d3.select('#' + id).selectAll('div').remove();
        var div = d3.select('#' + id);
        div.attr("align", "center");

        // Code to paint
    }
};
\end{lstlisting}

\section{MongoDB: a NoSQL Database}
\label{sec:mongodb}
MongoDB\footnote{https://www.mongodb.org/} is an open-source document-oriented NoSQL database distributed under the GNU Affero General Public License\footnote{http://www.gnu.org/licenses/agpl-3.0.html} and the Apache License\footnote{http://www.apache.org/licenses/LICENSE-2.0.html}, written in the programming language C. As a NoSQL database, instead of traditional table-based relational database MongoDB is structured into collections. Those collections are a set of BSON (Binary JSON) documents containing a set of fields or key-value pairs: keys are string and value cans be of so many types (string, integer, float, timestamp, etc.). That provides high performance, high availability, and automatic scaling. Figure [\ref{fig:mongodbdocument}] shows the possible similarities or equivalences between MongoDB and traditional relational databases.

\hspace{1cm}

\begin{figure}[ht!]
\centering
\includegraphics[width=200pt]{img/mongodbdocument.jpeg}
\caption[MongoDB data model]{Comparison between relational and MongoDB data models\footnotemark}
\label{fig:mongodbdocument}
\end{figure}

In MongoDB, the basic piece of data is called a document[\ref{fig:mongodbcollection}]. A major advantage in MongoDB is that documents do not have a predefined schema (\textit{flexible schema}). We can think of a document as a multidimensional array whose values could themselves be another array. In practical matters, MongoDB documents have a JSON array structure.

Furthermore, MongoDB is optimized for CRUD operations. You can store as much information as you need in a document without first defining its structure, and this data will be able to be queried. In order to retrieve one o more documents, you may run your own query specifying some criteria or conditions. A query may support search by field, range or conditional statements such as the existence or not of a key. This make the system highly scalable. 

\hspace{1cm}

\begin{figure}[ht!]
\centering
\includegraphics[width=300pt]{img/mongodbcollection.png}
\caption[MongoDB collection]{A collection of MongoDB documents\footnotemark}
\label{fig:mongodbcollection}
\end{figure}

\footnotetext{http://docs.mongodb.org/manual/core/crud-introduction/}
MongoDB also offers the possibility of replication into two or more replica sets, providing high availability. Every moment one of the replica sets works as the primary and replaces and updates the data of the replicas. When the primary fails, the secondary replica becomes principal. Additionally, MongoDB can run simultaneously over multiple servers, balancing the load between them and keeping those security replica sets and the system running in case of hardware failure.

MongoDB supports drivers for most common programming languages. Due to the fact that the structure of a document is similar to a JSON object and most of programming languages drivers support the management and conversion of JSON datatypes to language-specific structures, it is too easy to communicate and manipulate the data. In the case of this project, we use the PHP\footnote{http://php.net/manual/es/book.mongo.php} driver for MongoDB. 

\section{GeoServer}
\label{sec:geoserver}
GeoServer\footnote{http://geoserver.org/}~\cite{geoserverbeginners} is an open source software server written in Java that allows users to view and edit geospatial data. GeoServer functions as the reference implementation of the Open Geospatial Consortium Web Feature Service\footnote{http://www.opengeospatial.org/standards/wfs} standard, and also implements the Web Map Service\footnote{http://www.opengeospatial.org/standards/wms}, Web Coverage Service\footnote{http://www.opengeospatial.org/standards/wcs} and Web Processing Service\footnote{http://www.opengeospatial.org/standards/wps} specifications. 

Some of the main features of GeoServer are:

\begin{itemize}
\item Full compatible with the OGC specifications.
\item Easy installation and configuration (no large configuration files needed)
\item Multiple formats supported, such as PostGIS or Shapefile.
\item Multiple output formats supported, such as JPEG, GIF, PNG, SVG y GML.
\item ECQL query language support.
\end{itemize}

GeoServer also includes an administration UI[\ref{fig:geoserverui}] from which users can manage the stored data, observe and analyse the different attributes of the information as well as a preview of the different layers	for what GeoServer includes an integrated OpenLayers client. In our project we save our geospatial dataset into a GeoServer installation and display the information with maps integrated into Sefarad as a new widget developed with OpenLayers.js. 

\hspace{1cm}

\begin{figure}[ht!]
\centering
\includegraphics[width=400pt]{img/geoserverui.png}
\caption{Geoserver administration UI}
\label{fig:geoserverui}
\end{figure}

\clearpage

\section{Fuseki: RDF over HTTP server}
\label{sec:fuseki}
Fuseki\footnote{http://jena.apache.org/documentation/serving\_data/} is a SPARQL server. It provides REST-style SPARQL HTTP Update, SPARQL Query, and SPARQL Update using the SPARQL protocol over HTTP. We will use a Fuseki server installation for storing our own RDF files containing geoSPARQL data. 


\section{OpenLayers}
\label{sec:openlayers}
OpenLayers\footnote{http://openlayers.org/}~\cite{openlayerscookbook}\cite{openlayersstarter} is the most complete and powerful open source JavaScript library to create any kind of web mapping application. OpenLayers was originally developed and released by MetaCarta under a BSD license. 

In addition to offering a great set of components, such as maps, layers, or controls, OpenLayers offers access to a great number of data sources using many different data formats, implements many standards from Open Geospatial Consortium (OGC), and is compatible with almost all browsers. Because of this, OpenLayers allows us to display the geographical information stored in all major and common data servers into functional and interactive maps. This means the users can connect your client application to web services spread, add data from a bunch of raster and vector file formats such as GeoJSON and GML, and organize them in layers to create original web mapping applications.

\begin{figure}[ht!]
\centering
\includegraphics[width=400pt]{img/openlayersexample2.png}
\caption{OpenLayers map example with multiple layers}
\label{fig:openlayersexample}
\end{figure}

OpenLayers provides lots of controls such as pan, zoom, and query the map to build interactive maps which give users the possibility to actually explore the maps and the geospatial data display on them. OpenLayers allows you to include as many layers as you want, each representing a piece of information. Each layer can be customized with different colours, transparency, shadows, alive and clicking information, etc., and can be shown or hidden every moment. There are two kinds of layers in OpenLayers: base and non-base. Base layers are always visible and determines some of the essential properties of the map (zoom, center, etc.). A map can have more than one base layer but only one of them can be active at a time. 

In the case of GeoLinked Data, OpenLayers provides us the necessary tools to represent geographical information stored in GeoJSON\footnote{http://geojson.org/} in a map. GeoJSON is a format for encoding a variety of geographic data structures and supports multiple geometry types, such as \textit{Point, LineString, Polygon, MultiPoint, MultiLineString, and MultiPolygon}. 


\section{Grunt: The JavaScript Task Runner}
\label{sec:gruntjs}
GruntJS\footnote{http://gruntjs.com/}~\cite{automatewithgrunt} is a JavaScript task runner written with Node.js\footnote{http://nodejs.org/} used to automate predefined tasks to ease the development and integration of our project and to save time automating repetitive tasks.

Grunt provides lots of plugins that are installed and manage via npm\footnote{https://www.npmjs.org/}, Node.js package manager, which allows us to automate some manual repetitive tasks we run as part of our development or deployment process. Those plugins are labelled \textit{contrib} packages, which means they are branded as officially maintained and stable. Moreover, users share their own plugins and every one can easily create their own user-defined task plugin if there is no one for the task they want to automate.

To automate your project with Grunt, you must implement your Gruntfile.js configuration file and a package.json file. In the configuration file we indicate the tasks we want to automate and load the corresponding plugins with a simple command and configure them with JSON format. The package.json file is used to list grunt and the Grunt plugins your project needs as npm \textit{devDependencies}. 

Once the two mentioned files have been created, each time grunt is run it looks for a locally installed Grunt. When it is found, the CLI (Grunt's Command Line Interface) loads the local installation of the Grunt library, applies the configuration from your \textbf{Gruntfile}, and executes any tasks you've requested for it to run. 
