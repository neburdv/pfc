\contentsline {chapter}{Resumen}{V}{chapter*.3}
\contentsline {chapter}{Abstract}{VII}{chapter*.6}
\contentsline {chapter}{Agradecimientos}{IX}{chapter*.9}
\contentsline {chapter}{Contents}{XI}{section*.10}
\contentsline {chapter}{List of Figures}{XVII}{section*.12}
\contentsline {chapter}{List of Tables}{XIX}{section*.14}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.16}
\contentsline {section}{\numberline {1.1}Context}{3}{section.17}
\contentsline {section}{\numberline {1.2}Master thesis goals}{4}{section.25}
\contentsline {section}{\numberline {1.3}Structure of this Master Thesis}{4}{section.26}
\contentsline {chapter}{\numberline {2}Enabling Technologies}{7}{chapter.27}
\contentsline {section}{\numberline {2.1}Overview}{9}{section.30}
\contentsline {section}{\numberline {2.2}Linked Data in a Nutshell}{10}{section.38}
\contentsline {section}{\numberline {2.3}Sefarad}{13}{section.72}
\contentsline {section}{\numberline {2.4}MongoDB: a NoSQL Database}{15}{section.116}
\contentsline {section}{\numberline {2.5}GeoServer}{16}{section.127}
\contentsline {section}{\numberline {2.6}Fuseki: RDF over HTTP server}{18}{section.134}
\contentsline {section}{\numberline {2.7}OpenLayers}{18}{section.136}
\contentsline {section}{\numberline {2.8}Grunt: The JavaScript Task Runner}{19}{section.140}
\contentsline {chapter}{\numberline {3}Requirement Analysis}{21}{chapter.144}
\contentsline {section}{\numberline {3.1}Overview}{23}{section.145}
\contentsline {section}{\numberline {3.2}Actors dictionary}{23}{section.146}
\contentsline {section}{\numberline {3.3}Use cases}{24}{section.148}
\contentsline {subsection}{\numberline {3.3.1}Portal users use cases}{25}{subsection.150}
\contentsline {subsubsection}{\numberline {3.3.1.1}Edit a SPARQL query}{26}{subsubsection.151}
\contentsline {subsubsection}{\numberline {3.3.1.2}Run a SPARQL query}{27}{subsubsection.153}
\contentsline {subsubsection}{\numberline {3.3.1.3}Visual display of the information}{28}{subsubsection.155}
\contentsline {subsubsection}{\numberline {3.3.1.4}Keyword search}{29}{subsubsection.157}
\contentsline {subsubsection}{\numberline {3.3.1.5}Faceted search}{30}{subsubsection.159}
\contentsline {subsubsection}{\numberline {3.3.1.6}Log-in/Log-out}{31}{subsubsection.161}
\contentsline {subsubsection}{\numberline {3.3.1.7}Customize Sefarad}{32}{subsubsection.163}
\contentsline {subsubsection}{\numberline {3.3.1.8}Save own configuration}{33}{subsubsection.165}
\contentsline {subsubsection}{\numberline {3.3.1.9}Reset own configuration}{34}{subsubsection.167}
\contentsline {subsection}{\numberline {3.3.2}Admin use cases}{35}{subsection.169}
\contentsline {subsubsection}{\numberline {3.3.2.1}Security and users management}{35}{subsubsection.170}
\contentsline {subsubsection}{\numberline {3.3.2.2}Local datasets management}{36}{subsubsection.172}
\contentsline {subsection}{\numberline {3.3.3}Conclusions}{36}{subsection.174}
\contentsline {chapter}{\numberline {4}Architecture}{37}{chapter.175}
\contentsline {section}{\numberline {4.1}Introduction}{39}{section.176}
\contentsline {section}{\numberline {4.2}Architecture}{39}{section.177}
\contentsline {section}{\numberline {4.3}SPARQL Engine}{41}{section.179}
\contentsline {subsection}{\numberline {4.3.1}SPARQL Editor}{41}{subsection.180}
\contentsline {subsection}{\numberline {4.3.2}SPARQL queries executor}{43}{subsection.187}
\contentsline {section}{\numberline {4.4}Geo Proxy}{43}{section.189}
\contentsline {section}{\numberline {4.5}Local Server}{46}{section.253}
\contentsline {subsection}{\numberline {4.5.1}Fuseki and Virtuoso}{46}{subsection.255}
\contentsline {subsection}{\numberline {4.5.2}Geo Server}{46}{subsection.256}
\contentsline {section}{\numberline {4.6}Search and filtering module}{47}{section.258}
\contentsline {subsection}{\numberline {4.6.1}Keyword search}{48}{subsection.259}
\contentsline {subsection}{\numberline {4.6.2}Faceted search}{49}{subsection.262}
\contentsline {subsection}{\numberline {4.6.3}Geo filtering}{49}{subsection.263}
\contentsline {section}{\numberline {4.7}Model View View-Model}{50}{section.266}
\contentsline {subsection}{\numberline {4.7.1}Data Model}{51}{subsection.276}
\contentsline {subsection}{\numberline {4.7.2}Widgets layout}{52}{subsection.277}
\contentsline {subsubsection}{\numberline {4.7.2.1}Linear layout}{53}{subsubsection.278}
\contentsline {subsubsection}{\numberline {4.7.2.2}Accordion layout}{53}{subsubsection.280}
\contentsline {section}{\numberline {4.8}User management module}{54}{section.282}
\contentsline {subsection}{\numberline {4.8.1}Security: authentication and authorization}{54}{subsection.284}
\contentsline {subsubsection}{\numberline {4.8.1.1}MondoDB connection}{55}{subsubsection.285}
\contentsline {subsubsection}{\numberline {4.8.1.2}Session Manager}{56}{subsubsection.321}
\contentsline {subsubsection}{\numberline {4.8.1.3}User class}{59}{subsubsection.388}
\contentsline {subsection}{\numberline {4.8.2}MongoDB: settings and preferences}{61}{subsection.439}
\contentsline {subsubsection}{\numberline {4.8.2.1}Loading configuration}{62}{subsubsection.440}
\contentsline {subsubsection}{\numberline {4.8.2.2}Saving configuration}{63}{subsubsection.472}
\contentsline {subsubsection}{\numberline {4.8.2.3}Deleting configuration}{64}{subsubsection.506}
\contentsline {section}{\numberline {4.9}Setup module}{64}{section.531}
\contentsline {subsection}{\numberline {4.9.1}Custom installer}{65}{subsection.532}
\contentsline {subsection}{\numberline {4.9.2}Automation: Grunt.JS Task Runner}{66}{subsection.536}
\contentsline {chapter}{\numberline {5}Case Study}{71}{chapter.624}
\contentsline {section}{\numberline {5.1}Introduction}{73}{section.625}
\contentsline {section}{\numberline {5.2}European universities}{74}{section.629}
\contentsline {subsection}{\numberline {5.2.1}Query and retrieve the data: SPARQL}{74}{subsection.630}
\contentsline {subsection}{\numberline {5.2.2}Showing the data: results table}{76}{subsection.682}
\contentsline {subsection}{\numberline {5.2.3}Geographic representation: Openlayers map}{77}{subsection.685}
\contentsline {subsection}{\numberline {5.2.4}Filtering technologies}{78}{subsection.687}
\contentsline {section}{\numberline {5.3}Restaurants and Districts in Madrid}{79}{section.690}
\contentsline {subsection}{\numberline {5.3.1}Download and process the information}{79}{subsection.693}
\contentsline {subsection}{\numberline {5.3.2}SPARQL and Fuseki}{80}{subsection.697}
\contentsline {subsection}{\numberline {5.3.3}Faceted search}{83}{subsection.798}
\contentsline {subsection}{\numberline {5.3.4}Openlayers and GeoJSON}{84}{subsection.801}
\contentsline {section}{\numberline {5.4}Slovakian dataset}{85}{section.826}
\contentsline {subsection}{\numberline {5.4.1}Protected sites dataset}{85}{subsection.829}
\contentsline {subsection}{\numberline {5.4.2}SPARQL Query and results data}{90}{subsection.834}
\contentsline {subsection}{\numberline {5.4.3}OpenStreet Map: GeoJSON representation}{92}{subsection.917}
\contentsline {section}{\numberline {5.5}SmartOpenData parcels dataset}{94}{section.937}
\contentsline {subsection}{\numberline {5.5.1}Parcels data scheme}{95}{subsection.940}
\contentsline {subsection}{\numberline {5.5.2}GeoServer and Openlayers}{96}{subsection.942}
\contentsline {subsection}{\numberline {5.5.3}Geo-filtering: ECQL}{97}{subsection.955}
\contentsline {chapter}{\numberline {6}Conclusions and future lines}{99}{chapter.957}
\contentsline {section}{\numberline {6.1}Project outcomes}{101}{section.958}
\contentsline {section}{\numberline {6.2}Achieved goals}{102}{section.959}
\contentsline {section}{\numberline {6.3}Conclusions}{102}{section.961}
\contentsline {section}{\numberline {6.4}Future work}{103}{section.963}
\contentsline {chapter}{\numberline {A}Installing and configuring Sefarad}{105}{appendix.965}
\contentsline {section}{\numberline {A.1}Installation}{107}{section.966}
\contentsline {subsection}{\numberline {A.1.1}Requirements}{107}{subsection.967}
\contentsline {subsection}{\numberline {A.1.2}Installation steps}{107}{subsection.972}
\contentsline {chapter}{\numberline {B}User Manual}{109}{appendix.988}
\contentsline {section}{\numberline {B.1}Create new widget}{111}{section.989}
\contentsline {chapter}{Bibliography}{112}{section*.1022}
